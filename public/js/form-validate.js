$(document).ready(function () {
    //Máscara Valor Monetário
    $('form').submit(function (e) {
        var isValid = true;
        // Tratamento dos dados
        var name = $('#name').val();
        var amount = $('#amount').val();
        var quantity = $('#quantity').val();
        amount = amount.replace('R$', '');
        amount = amount.replace('.', '');
        amount = amount.replace(',', '.');
        if (!(/^([a-zA-ZáàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ'\s]*)$/.test(name))) {
            $("#error-name").html("Nome inválido!");
            isValid = false;
        }
        if (!($.isNumeric(amount)) || (amount < 0)) {
            $("#error-amount").html("Preço inválido!");
            isValid = false;
        }else{
            $('#amount').val(amount);
        }
        if (!($.isNumeric(quantity)) || quantity < 0) {
            $("#error-quantity").html("Estoque inválido!");
            isValid = false;
        }
        if (!isValid) {
            e.preventDefault();
        }
    });
});