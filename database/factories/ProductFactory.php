<?php

use App\Models\Product;

$faker = Faker\Factory::create('pt_BR');
$factory->define(Product::class, function() use ($faker) {
    return [
        'name' => $faker->name,
        'amount' => $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 1000),
        'quantity' => $faker->numberBetween($min = 0, $max = 100)
    ];
});
