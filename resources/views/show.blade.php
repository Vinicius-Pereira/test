@extends('layout')
@section('content')

    <a class="waves-effect waves-light btn-small btn-back right" href="{{route('products.index')}}"><i class="material-icons left">chevron_left</i>Voltar</a>
    <h4>{{$resource->name}}</h4>
    <div class="row">
        <div class="input-field col s12">
            <input disabled value="{{$resource->name}}" id="disabled" type="text" class="validate">
            <label for="disabled">Nome:</label>
        </div>
    </div>
    <div class="row">
        <div class="input-field col s6">
            <input disabled value="{{$resource->amount}}" id="disabled" type="text" class="validate">
            <label for="disabled">Preço:</label>
        </div>
        <div class="input-field col s6">
            <input disabled value="{{$resource->quantity}}" id="disabled" type="text" class="validate">
            <label for="disabled">Quantidade em Estoque:</label>
        </div>
    </div>
@endsection