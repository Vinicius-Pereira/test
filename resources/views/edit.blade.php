@extends('layout')
@section('content')

    <a class="waves-effect waves-light btn-small btn-back right" href="{{route('products.index')}}"><i
                class="material-icons left">chevron_left</i>Voltar</a>
    <h4>Editar - {{$resource->name}}</h4>
    <form action="{{route('products.update', ['id' => $resource->id])}}" method="post">
        {{ csrf_field() }}
        {{ method_field('put') }}

        <div class="row">
            <div class="input-field col s12">
                <input name="name" id="name" value="{{$resource->name}}" type="text" class="validate" data-length="255">
                <label for="name">Nome:</label>
                <div id="error-name" class="row error">
                    @if($errors->has('name'))
                        Nome inválido!
                    @endif
                </div>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s6">
                <input name="amount" id="amount" value="{{$resource->amount}}" type="text" class="validate">
                <label for="amount">Preço:</label>
                <div id="error-amount" class="row error">
                    @if($errors->has('amount'))
                        Preço inválido!
                    @endif
                </div>
            </div>
            <div class="input-field col s6">
                <input name="quantity" id="quantity" value="{{$resource->quantity}}" type="number" min="0" step="1" class="validate">
                <label for="quantity">Quantidade em Estoque:</label>
                <div id="error-quantity" class="row error">
                    @if($errors->has('quantity'))
                        Estoque inválido!
                    @endif
                </div>
            </div>
        </div>
        <div class="row">
            <button class="waves-effect waves-light btn-large right" type="submit"><i class="material-icons left">save</i>Salvar</button>
        </div>
    </form>

    {{-- Importa máscara valor monetário --}}
    <script src="{{asset('js/jquery.priceformat.min.js')}}"></script>
    {{-- Máscaras campos formulário --}}
    <script src="{{asset('js/form-mask.js')}}"></script>
    {{-- Validação de envio do formulário --}}
    <script src="{{asset('js/form-validate.js')}}"></script>
@endsection