<html>
<head>
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="{{asset('css/materialize.min.css')}}"/>

    <!-- Icons Materialize -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- Styles -->
    <link href="{{asset('css/styles.css')}}" rel="stylesheet">

    <!-- Compiled and minified JavaScript Materialize -->
    <script src="{{asset('js/materialize.min.js')}}"></script>

    <!-- Compiled and minified JavaScript JQuery -->
    <script src="{{asset('js/jquery.min.js')}}"></script>

    @if(!empty($message))
        <span id="message">{{$message}}</span>
        <script src="{{asset('js/toast.js')}}"></script>
        @endif

    <title>INDEX - Teste Vinicius S Pereira</title>
</head>
<body>
<nav>
    <div class="nav-wrapper">
        <a href="{{route('products.index')}}" class="brand-logo center">VTest</a>
    </div>
</nav>
<ul id="slide-out" class="sidenav sidenav-fixed">
    <li id="top-nav"><span id="menu-text">Menu</span></li>
    <li><a href="{{route('products.index')}}">Produtos</a></li>
</ul>
<a href="#" data-target="slide-out" class="sidenav-trigger"><i class="material-icons">menu</i></a>

<main>
    <div>
        <div class="row">
            <div class="col s12 m12 l12 xl12">
                <div class="card-panel">
                    @yield('content')
                </div>
            </div>
        </div>
    </div>
</main>
</body>
<footer>

</footer>
</html>